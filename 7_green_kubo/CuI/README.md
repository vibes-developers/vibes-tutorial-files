CuI ab initio MD data
===

This data was produced for the work ["*Ab initio* Green-Kubo simulations of heat transport in solids: Method and implementation"](https://doi.org/10.1103/PhysRevB.107.224304) [[1](#References)].

## Data

- `nvt.1`: 1000 steps NVT MD at 300K
- `nvt.2`: 2000 steps NVT MD at 300K with thermally expanded lattice
- `nve.1`: 12000 steps NVE with 1 defect
- `nve.3`: 12000 steps NVE with up to 3 defects
- `md_summary.pdf`: summary plot for the trajectory

## Format

Data is in FHI-vibes trajectory format. These are HDF5/NetCDF files that can be parsed e.g. using `xarray` or `h5py`. The data arrays inside are basically wrapped numpy arrays. The arrays of interest here are:

- `positions`: atom positions in Angstrom
- `forces`: forces in eV/Angstrom
- `energy_potential`: DFT potential energy in eV
- `stress_potential`: DFT potential stress in eV/Angstrom^3
- `sigma_per_sample`: anharmonicity measure per sample [[2, 3](#References)]
- Bonus: `forces_harmonic` and `energy_potential_harmonic` are the harmonic forces and energies.

## References

​    [[1] F. Knoop, M. Scheffler, and C. Carbogno, Phys. Rev. B **107**, 224304 (2023).  ](https://doi.org/10.1103/PhysRevB.107.224304)

​    [[2] F. Knoop *et al.*, Phys Rev Mater **4**, 083809 (2020).  ](https://dx.doi.org/10.1103/physrevmaterials.4.083809)

​    [[3] F. Knoop *et al.*, Phys. Rev. Lett. **130**, 236301 (2023).  ](https://dx.doi.org/10.1103/physrevlett.130.236301)